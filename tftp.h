#ifndef _TFTPH
#define _TFTPH

typedef enum tftp_state {
  TFTP_RQ_SENT,
  TFTP_DATA_RCVD,
  TFTP_ACK_RCVD,
} tftp_state_t;

typedef enum tftp_direction {
  TFTP_SEND,
  TFTP_RECV,
} tftp_direction_t;

typedef struct tftp_conn {
  uint16_t local_tid;
  uint16_t remote_tid;
  uint8_t *filename;
  uint16_t curr_block;
  /* TODO: file handles */
  tftp_direction_t direction;
  /* TODO: timer handle? */
} tftp_conn_t;

#endif	/* _TFTPH */
