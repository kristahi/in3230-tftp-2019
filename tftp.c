#include <stdio.h>
#include <string.h>
#include <uv.h>
#include "tftp.h"

typedef struct tftp_settings {
  uint8_t *filename;
  tftp_direction_t direction;
  uint8_t *hostname;
} tftp_settings_t;

static int tftp_init(tftp_settings_t *settings);
static int tftp_cmdparse(int argc, char *argv[], tftp_settings_t *settings);

int main(int argc, char *argv[])
{
  uv_loop_t loop;
  tftp_settings_t settings;

  if (tftp_cmdparse(argc, argv, &settings)) {
    return 1;
  }

  uv_loop_init(&loop);

  /* Init work */
  if (tftp_init(&settings)) {
    /* Wait for stuff to happen */
    uv_run(&loop, UV_RUN_DEFAULT);
  }

  uv_loop_close(&loop);

  return 0;
}

static int tftp_init(tftp_settings_t *settings)
{
  /* Open socket */
}

static int tftp_cmdparse(int argc, char *argv[], tftp_settings_t *settings)
{
  /* Usage: tftp (put|get) hostname filename */

  if (argc < 4) {
    fprintf(stderr, "Not enough parameters.\n");
    return 1;
  }
  
  if (!strcmp(argv[1], "put")) {
    settings->direction = TFTP_SEND;
  } else if (!strcmp(argv[1], "get")) {
    settings->direction = TFTP_RECV;
  } else {
    fprintf(stderr, "Unknown request mode %s\n", argv[1]);
    return 1;
  }
  
  settings->hostname = argv[2]; /* On main's stack */
  settings->filename = argv[3];

  return 0;
}
